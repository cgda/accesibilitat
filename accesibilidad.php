<!-- labs.iam.cat/~a15antcernog/formulari -->
<!DOCTYPE HTML>  
<html lang="en"> 
<head>
  <title>Form Validation</title>     
 <meta charset="utf-8">
<style>
.error {color: #FF0000;}
</style>
</head>
<body>  

<?php
// define variables and set to empty values
$nameErr = $emailErr = $genderErr = $websiteErr = "";
$name = $email = $gender = $comment = $website = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } else {
    $name = test_input($_POST["name"]);
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
  }
    
  if (empty($_POST["website"])) {
    $website = "";
  } else {
    $website = test_input($_POST["website"]);
  }

  if (empty($_POST["comment"])) {
    $comment = "";
  } else {
    $comment = test_input($_POST["comment"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  } else {
    $gender = test_input($_POST["gender"]);
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
  
 if (!$_POST ){
?>

<h1>PHP Form Validation Example</h1>
<p><span class="error">* required field.</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  <fieldset>
  <label for="name">Name:</label>
  <input type="text" name="name" id="name" value="<?php echo $name;?>" required="required">
  <span class="error">* <?php echo $nameErr;?></span>
  <br><br>
  <label for="email">E-mail:</label>
  <input type="text" name="email" id="email" value="<?php echo $email;?>" required="required">
  <span class="error">* <?php echo $emailErr;?></span>
  <br><br>
  <label for="web">Website:</label>
  <input type="text" name="website" id="web" value="<?php echo $website;?>">
  <span class="error"><?php echo $websiteErr;?></span>
  <br><br>
  <label for="comment">Comment:</label>
  <textarea name="comment" id="comment" rows="5" cols="40" ><?php echo $comment;?></textarea>
  <br><br>
  <label for="curso">Curso:</label>
  <select name="curso" id="curso" form="curso">
    <option value="eso">ESO</option>
    <option value="batx">Batxillerat</option>
    <option value="smx">SMX</option>
    <option value="dam">DAM</option>
    <option value="asix">ASIX</option>
    <option value="daw">DAW</option>
    <option value="altres">Altres</option>
  </select>
   <br><br>
<fieldset>
  <legend>Gender</legend>
  <label for = "female">Female</label>
  <input title="female" type="radio" name="gender" id="female" value="female" required="required" <?php if($gender=="female")echo "checked='checked'";?>>
  <label for = "male">Male</label>
  <input id = "male" title="male" type="radio" name="gender" value="male" <?php if($gender=="male")echo "checked='checked'";?>>Male
  <span class="error">* <?php echo $genderErr;?></span>
</fieldset>  
<br><br>
  </fieldset>
  <input type="submit" name="submit" value="Submit">  
</form>

<?php }else{
               
echo "<h2>Your Input:</h2>";
echo $name;
echo "<br>";
echo $email;
echo "<br>";
echo $website;
echo "<br>";
echo $comment;
echo "<br>";
echo $gender;
 }
?>

</body>
</html>
